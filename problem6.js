let getFirstHobby = (usersData) => {
  let firstHobbyArr = [];

  for (let user of usersData) {
    firstHobbyArr.push(user.hobbies[0]);
  }

  if (firstHobbyArr.length > 0) {
    return firstHobbyArr;
  } else {
    return "hobbies are not defined";
  }
};

module.exports = getFirstHobby;
