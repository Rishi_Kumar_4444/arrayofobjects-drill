let getUserByAge = (usersData, age) => {
  for (let user of usersData) {
    if (user.age > age || user.age === age) {
      console.log(`${user.name} age is ${user.age} and email is ${user.email}`);
    }
  }
};

module.exports = getUserByAge;
