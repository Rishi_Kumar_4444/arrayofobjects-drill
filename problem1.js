let getEmails = (usersData) => {
  let userEmails = [];

  for (let user of usersData) {
    userEmails.push(user.email);
  }

  if (userEmails.length > 0) {
    return userEmails;
  } else {
    return "users not found";
  }
};

module.exports = getEmails;
