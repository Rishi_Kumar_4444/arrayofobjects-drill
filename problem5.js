let getAges = (usersData) => {
  let agesArray = [];

  for (let user of usersData) {
    agesArray.push(user.age);
  }

  if (agesArray.length > 0) {
    return agesArray;
  } else {
    return "age is not defined";
  }
};

module.exports = getAges;
