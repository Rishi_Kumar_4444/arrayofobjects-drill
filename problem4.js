const getUserByIndex = (usersData, index) => {
  for (let user of usersData) {
    if (user.id === index) {
      return `${user.name} is from ${user.city}`;
    }
  }

  return " user not found";
};

module.exports = getUserByIndex;
