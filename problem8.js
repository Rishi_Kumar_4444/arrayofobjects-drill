let getCityCountry = (userData) => {
  for (let user of userData) {
    console.log(`${user.name} lives in ${user.city}, ${user.country}`);
  }
};

module.exports = getCityCountry;
