let getHobbies = (usersData, ageInput) => {
  let hobbies = [];

  for (let user of usersData) {
    if (user.age === ageInput) {
      for (let hobby of user.hobbies) {
        hobbies.push(hobby);
      }
    }
  }

  if (hobbies.length > 0) {
    return hobbies;
  } else {
    return `no user found with given age`;
  }
};

module.exports = getHobbies;
