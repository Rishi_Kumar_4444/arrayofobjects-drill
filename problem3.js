const getNamesByLocation = (usersData, country) => {
  let users = [];

  for (let user of usersData) {
    if (user.isStudent && user.country === country) {
      users.push(user);
    }
  }

  if (users.length > 0) {
    return users;
  } else {
    return "no users found";
  }
};

module.exports = getNamesByLocation;
